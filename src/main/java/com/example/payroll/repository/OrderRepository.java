package com.example.payroll.repository;

import com.example.payroll.model.Order1;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order1, Long> {
}
