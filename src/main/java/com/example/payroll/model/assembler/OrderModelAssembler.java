package com.example.payroll.model.assembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.example.payroll.controller.OrderController;
import com.example.payroll.model.Order1;
import com.example.payroll.model.Status;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class OrderModelAssembler implements RepresentationModelAssembler<Order1, EntityModel<Order1>> {

    @Override
    public EntityModel<Order1> toModel(Order1 order1) {

        // Unconditional links to single-item resource and aggregate root

        EntityModel<Order1> orderModel = EntityModel.of(order1,
                linkTo(methodOn(OrderController.class).one(order1.getId())).withSelfRel(),
                linkTo(methodOn(OrderController.class).all()).withRel("orders"));

        // Conditional links based on state of the order

        if (order1.getStatus() == Status.IN_PROGRESS) {
            orderModel.add(linkTo(methodOn(OrderController.class).cancel(order1.getId())).withRel("cancel"));
            orderModel.add(linkTo(methodOn(OrderController.class).complete(order1.getId())).withRel("complete"));
        }

        return orderModel;
    }
}