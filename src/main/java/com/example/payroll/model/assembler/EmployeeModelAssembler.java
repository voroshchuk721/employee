package com.example.payroll.model.assembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.example.payroll.controller.EmployeeController;
import com.example.payroll.model.Employee1;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public
class EmployeeModelAssembler implements RepresentationModelAssembler<Employee1, EntityModel<Employee1>> {

    @Override
    public EntityModel<Employee1> toModel(Employee1 employee1) {

        return EntityModel.of(employee1,
                linkTo(methodOn(EmployeeController.class).one(employee1.getId())).withSelfRel(),
                linkTo(methodOn(EmployeeController.class).all()).withRel("employees"));
    }
}