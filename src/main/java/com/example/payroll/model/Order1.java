package com.example.payroll.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Order1 {

    @Id
    @GeneratedValue
    private Long id;

    private String description;
    private Status status;

    public Order1() {
    }

    public Order1(String description, Status status) {

        this.description = description;
        this.status = status;
    }


    public Long getId() {
        return this.id;
    }

    public String getDescription() {
        return this.description;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof Order1))
            return false;
        Order1 order1 = (Order1) o;
        return Objects.equals(this.id, order1.id) && Objects.equals(this.description, order1.description)
                && this.status == order1.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.description, this.status);
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + this.id + ", description='" + this.description + '\'' + ", status=" + this.status + '}';
    }
}