package com.example.payroll.controller;


import com.example.payroll.model.assembler.OrderModelAssembler;
import com.example.payroll.exception.OrderNotFoundException;
import com.example.payroll.model.Order1;
import com.example.payroll.model.Status;
import com.example.payroll.repository.OrderRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class OrderController {

    private final OrderRepository orderRepository;
    private final OrderModelAssembler assembler;

    OrderController(OrderRepository orderRepository, OrderModelAssembler assembler) {

        this.orderRepository = orderRepository;
        this.assembler = assembler;
    }

    @GetMapping("/orders")
    public CollectionModel<EntityModel<Order1>> all() {

        List<EntityModel<Order1>> orders = orderRepository.findAll().stream() //
                .map(assembler::toModel) //
                .collect(Collectors.toList());

        return CollectionModel.of(orders, //
                linkTo(methodOn(OrderController.class).all()).withSelfRel());
    }

    @GetMapping("/orders/{id}")
    public EntityModel<Order1> one(@PathVariable Long id) {

        Order1 order1 = orderRepository.findById(id) //
                .orElseThrow(() -> new OrderNotFoundException(id));

        return assembler.toModel(order1);
    }

    @PostMapping("/orders")
    ResponseEntity<EntityModel<Order1>> newOrder(@RequestBody Order1 order1) {

        order1.setStatus(Status.IN_PROGRESS);
        Order1 newOrder1 = orderRepository.save(order1);

        return ResponseEntity //
                .created(linkTo(methodOn(OrderController.class).one(newOrder1.getId())).toUri()) //
                .body(assembler.toModel(newOrder1));
    }

    @PutMapping("/orders/{id}/complete")
    public ResponseEntity<?> complete(@PathVariable Long id) {

        Order1 order1 = orderRepository.findById(id) //
                .orElseThrow(() -> new OrderNotFoundException(id));

        if (order1.getStatus() == Status.IN_PROGRESS) {
            order1.setStatus(Status.COMPLETED);
            return ResponseEntity.ok(assembler.toModel(orderRepository.save(order1)));
        }

        return ResponseEntity //
                .status(HttpStatus.METHOD_NOT_ALLOWED) //
                .header(HttpHeaders.CONTENT_TYPE, MediaTypes.HTTP_PROBLEM_DETAILS_JSON_VALUE) //
                .body(Problem.create() //
                        .withTitle("Method not allowed") //
                        .withDetail("You can't complete an order that is in the " + order1.getStatus() + " status"));
    }

    @DeleteMapping("/orders/{id}/cancel")
    public ResponseEntity<?> cancel(@PathVariable Long id) {

        Order1 order1 = orderRepository.findById(id) //
                .orElseThrow(() -> new OrderNotFoundException(id));

        if (order1.getStatus() == Status.IN_PROGRESS) {
            order1.setStatus(Status.CANCELLED);
            return ResponseEntity.ok(assembler.toModel(orderRepository.save(order1)));
        }

        return ResponseEntity //
                .status(HttpStatus.METHOD_NOT_ALLOWED) //
                .header(HttpHeaders.CONTENT_TYPE, MediaTypes.HTTP_PROBLEM_DETAILS_JSON_VALUE) //
                .body(Problem.create() //
                        .withTitle("Method not allowed") //
                        .withDetail("You can't cancel an order that is in the " + order1.getStatus() + " status"));
    }
}

