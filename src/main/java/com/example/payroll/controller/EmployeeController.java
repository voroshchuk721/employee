package com.example.payroll.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.example.payroll.model.assembler.EmployeeModelAssembler;
import com.example.payroll.exception.EmployeeNotFoundException;
import com.example.payroll.model.Employee1;
import com.example.payroll.repository.EmployeeRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public
class EmployeeController {

    private final EmployeeRepository repository;

    private final EmployeeModelAssembler assembler;

    public EmployeeController(EmployeeRepository repository, EmployeeModelAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/employees")
    public CollectionModel<EntityModel<Employee1>> all() {

        List<EntityModel<Employee1>> employees = repository.findAll().stream() //
                .map(assembler::toModel) //
                .collect(Collectors.toList());

        return CollectionModel.of(employees, linkTo(methodOn(EmployeeController.class).all()).withSelfRel());
    }
    // end::get-aggregate-root[]

    @PostMapping("/employees")
    ResponseEntity<?> newEmployee(@RequestBody Employee1 newEmployee1) {

        EntityModel<Employee1> entityModel = assembler.toModel(repository.save(newEmployee1));

        return ResponseEntity //
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }

    // Single item


    @GetMapping("/employees/{id}")
    public EntityModel<Employee1> one(@PathVariable Long id) {

        Employee1 employee1 = repository.findById(id) //
                .orElseThrow(() -> new EmployeeNotFoundException(id));

        return assembler.toModel(employee1);
    }

    @PutMapping("/employees/{id}")
    ResponseEntity<?> replaceEmployee(@RequestBody Employee1 newEmployee1, @PathVariable Long id) {

        Employee1 updatedEmployee1 = repository.findById(id) //
                .map(employee -> {
                    employee.setName(newEmployee1.getName());
                    employee.setRole(newEmployee1.getRole());
                    return repository.save(employee);
                }) //
                .orElseGet(() -> {
                    newEmployee1.setId(id);
                    return repository.save(newEmployee1);
                });

        EntityModel<Employee1> entityModel = assembler.toModel(updatedEmployee1);

        return ResponseEntity //
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }

    @DeleteMapping("/employees/{id}")
    ResponseEntity<?> deleteEmployee(@PathVariable Long id) {

        repository.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}